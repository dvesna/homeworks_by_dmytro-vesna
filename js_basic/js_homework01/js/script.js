const userName = prompt('Input your name', 'user');
const userAge = parseInt(prompt('Input your Age', '18'));

const messageAccessDenied = "You are not allowed to visit this website";
const messageUserWelcome = `Welcome, ${userName}`;

if (userAge < 18) {
    alert(messageAccessDenied)
} else if (userAge <= 22) {
    let ageConfirmation = confirm("Are you sure you want to continue?");
    if (ageConfirmation) {
        alert(messageUserWelcome);
    } else {
        alert(messageAccessDenied);
    }
} else {
    alert(messageUserWelcome);
};

