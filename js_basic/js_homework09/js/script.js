const allTabsContainer = document.querySelector('.tabs');
const allTabs = document.querySelectorAll('.tabs .tabs-title');
const allTextContainer = document.querySelectorAll('.tabs-content li');

allTabs.forEach((el, index) => {
    allTextContainer[index].hidden = !el.classList.contains('active');
});

allTabsContainer.addEventListener('click', (event) => {
    const clickedItem = (event.target);
    allTabs.forEach((el, index) => {
        el.classList.remove('active');
        if (el === clickedItem) {
            allTabs[index].classList.add('active');
            allTextContainer.forEach(e => {
                e.hidden = true
            });
            allTextContainer[index].hidden = false;
        }
    });
});