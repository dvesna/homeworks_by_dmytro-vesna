let someArray = ['some string', 23, true, null, NaN, {}, {}, {}, 'some string', 23, true, null, NaN, 'some string', 23, true, null, NaN];

const filterBy = function (arrayForFilter = someArray, filterType = 'number') {
    return arrayForFilter.filter((item) =>  typeof item !== filterType);
};

console.log('Array before filter ->',someArray);

console.log('Array after filter ->', filterBy());
