const userNumber = +prompt('Enter an integer', '1');

if (userNumber >= 5) {
    for (let i = 0; i <= userNumber; i++) {
        if (i % 5 === 0) {
            console.log(i);
        }
    }
} else {
    console.log('Sorry, no numbers');
}
