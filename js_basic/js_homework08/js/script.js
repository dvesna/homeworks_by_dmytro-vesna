const inputPriceField = document.getElementById('price-input');
const inputResultWrapper = document.getElementById('result-wrapper');
console.log(inputResultWrapper, inputPriceField);


inputPriceField.addEventListener('change', (e) => {

    const currentPrice = document.createElement('span');
    const errorPrice = document.createElement('span');
    const clearPrice = document.createElement('span');
    clearPrice.textContent = '(x)';
    const enteredPrice = inputPriceField.value;
    console.log(clearPrice.outerHTML);

    if (enteredPrice < 0) {
        errorPrice.remove();
        inputResultWrapper.innerHTML = '';
        inputPriceField.after(errorPrice);
        errorPrice.classList.add('error-price');
        errorPrice.innerText = `Please enter correct price `;
        inputPriceField.classList.add('error');
        errorPrice.appendChild(clearPrice);
    } else {
        inputPriceField.classList.remove('error');
        errorPrice.remove();
        inputResultWrapper.innerHTML = '';
        currentPrice.classList.add('current-price');
        currentPrice.innerText = `Текущая цена: ${enteredPrice} `;
        currentPrice.appendChild(clearPrice);
        inputResultWrapper.appendChild(currentPrice);
    }
    clearPrice.addEventListener('click', () => {
        inputResultWrapper.innerHTML = '';
        errorPrice.remove();
        inputPriceField.value = '';
        inputPriceField.classList.remove('error');
    })
});

