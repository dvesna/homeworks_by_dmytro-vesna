const numberRequest = function (number) {
    let userNumber = prompt('Enter a number', '0');
    while (isNaN(userNumber) || userNumber === '' || userNumber === null) {
        alert(`${userNumber} - IS NOT A NUMBER`);
        userNumber = prompt('Enter a NUMBER please', '0');
    }
    return userNumber;
};

const firstNumber = numberRequest();
const secondNumber = numberRequest();

const mathAction = prompt('enter an action for calculation', '+');
while (mathAction !== '+' && mathAction !== '-' && mathAction !== '*' && mathAction !== '/') {
    mathAction = prompt(`${mathAction} - IS NOT VALID ACTION!. Enter an suggested action again [+],[-],[*],[/]`, '+');
}

const calculator = function (firstOperand, secondOperand, calcAction) {

    let result = 0;

    switch (calcAction) {
        case '+':
            result = +firstOperand + +secondOperand;
            break;
        case '-':
            result = firstOperand - secondOperand;
            break;
        case '*':
            result = firstOperand * secondOperand;
            break;
        case '/':
            result = firstOperand / secondOperand;
            break;
    }
    return result;
};

console.log(calculator(firstNumber,secondNumber,mathAction));