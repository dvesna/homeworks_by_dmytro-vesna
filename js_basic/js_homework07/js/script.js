let templist = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

function listCreator(arrArg) {
    let list = document.createElement('ul');
    document.querySelector('script').before(list);

    function createList(arg) {
        let createdArray = arg.map((elem) => {
            let li = document.createElement('li');
            li.innerText = elem;
            document.querySelector('ul').appendChild(li);
        })
    }

    console.log(createList(arrArg));
}

console.log(listCreator(templist));