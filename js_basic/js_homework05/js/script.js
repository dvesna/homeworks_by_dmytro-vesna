const userName = prompt('Enter your first name');
const userSurname = prompt('Enter your second name');
const userBirthDate = prompt('Enter your birth date as dd.mm.yyyy');

const generateLogin = function (a, b) {
    return (a[0] + b).toLowerCase();
};

const generatePassword = function (a, b, c) {
    return (a[0].toUpperCase() + b.toLowerCase() + c);
};

const generateAge = function (birthDateInfo) {
    let currentDate = new Date();
    let birthDay = new Date(birthDateInfo.month + '.' + birthDateInfo.day + '.' + birthDateInfo.year);
    let userAge = currentDate.getFullYear() - birthDay.getFullYear();
    if (currentDate.getMonth() < birthDay.getMonth() || (currentDate.getDate() < birthDay.getDate() && currentDate.getMonth() === birthDay.getMonth())) {
        return userAge - 1;
    }
    return userAge;
};

const createNewUser = function (firstName, lastName, date) {
    return {
        firstName: firstName,
        lastName: lastName,
        birthDate: {
            day: date.split(".")[0],
            month: date.split(".")[1],
            year: date.split(".")[2],
        },

        getLogin: function () {
            return generateLogin(this.firstName, this.lastName);
        },

        getPassword: function () {
            return generatePassword(this.firstName, this.lastName, this.birthDate.year);
        },

        getAge: function () {
            return generateAge(this.birthDate);
        }
    }
};

const testUser = createNewUser(userName, userSurname, userBirthDate);

console.log(testUser);
console.log(testUser.getLogin());
console.log(testUser.getPassword());
console.log(testUser.getAge());