const pacMan = document.querySelector('.pacman');

document.addEventListener('keydown', (event) => {
    console.log(event.clientX);
    let clientWidth = window.innerWidth - 150;
    let clientHeight = window.innerHeight - 200;
    let pacmanPosX = pacMan.style.left.split('px')[0];
    let pacmanPosY = pacMan.style.top.split('px')[0];
    let pacmanSize = pacMan.style.width.split('px')[0];
    pacMan.style.transition = `left 0.3s ease-out,
                               top 0.3s ease-out,
                               rotate 0.3s ease-out,
                               rotate3d 0.5s ease-out`;
    switch (event.key) {
        case 'ArrowRight':
            pacMan.style.transform = "rotate(0deg)";
            if (pacmanPosX >= 0 && pacmanPosX < (clientWidth - pacmanSize - 50)) {
                pacMan.style.left = `${+pacmanPosX + 50}px`
            }
            break;
        case 'ArrowLeft':
            pacMan.style.transform = "rotate3d(0, -180, 0, 180deg)";
            if (pacmanPosX > 0) {
                pacMan.style.left = `${+pacmanPosX - 50}px`
            }
            break;
        case 'ArrowDown':
            pacMan.style.transform = "rotate(90deg)";
            if (pacmanPosY >= 0 && pacmanPosY < (clientHeight - pacmanSize - 50)) {
                pacMan.style.top = `${+pacmanPosY + 50}px`
            }
            break;
        case 'ArrowUp':
            pacMan.style.transform = "rotate(270deg)";
            if (pacmanPosY > 0) {
                pacMan.style.top = `${+pacmanPosY - 50}px`
            }
            break;
    }
});

