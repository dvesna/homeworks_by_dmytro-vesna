const form = document.querySelector('.password-form');
const topIcon = document.getElementById('top-icon');
const bottomIcon = document.getElementById('bottom-icon');
const errorMessage = document.createElement('p');

const togglePasswordShow = function (event) {
    const openEye = 'fa-eye';
    const closedEye = 'fa-eye-slash';
    event.target.classList.replace(openEye, closedEye) || event.target.classList.replace(closedEye, openEye);
    if (event.target.classList.contains(closedEye)) {
        if (event.target === topIcon) {
            topInput.setAttribute('type', 'text');
        }
        if (event.target === bottomIcon) {
            bottomInput.setAttribute('type', 'text');
        }
    }
    if (event.target.classList.contains(openEye)) {
        if (event.target === topIcon) {
            topInput.setAttribute('type', 'password');
        }
        if (event.target === bottomIcon) {
            bottomInput.setAttribute('type', 'password');
        }
    }
    if (event.target === submitBtn) {
        if (topInput.value === bottomInput.value) {
            errorMessage.remove();
            setTimeout(() => {alert('You are welcome')}, 100);
        } else {
            errorMessage.classList.add('error-message');
            errorMessage.innerText = 'Нужно ввести одинаковые значения';
            bottomInput.after(errorMessage);
        }
    }
};


form.addEventListener('click', (event) => {
    togglePasswordShow(event);
});


