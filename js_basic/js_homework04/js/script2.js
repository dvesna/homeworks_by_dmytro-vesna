const userName = prompt('Enter your first name');
const userSurname = prompt('Enter your second name');

let counter = 0;


class User {
    constructor(aName = "User", bName = `${counter++}`) {
        this.firstName = aName;
        this.lastName = bName
    }
    getLogin () {
        return generateLogin(this.firstName, this.lastName);
    }
}

const newUser = new User(userName, userSurname);
console.log(newUser);
console.log(newUser.getLogin());
