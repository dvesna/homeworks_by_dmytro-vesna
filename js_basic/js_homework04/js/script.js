const userName = prompt('Enter your first name');
const userSurname = prompt('Enter your second name');

const generateLogin = function (a, b) {
    return (a[0] + b).toLowerCase();
};

const createNewUser = function (firstName, lastName) {
    return {
        firstName: firstName,
        lastName: lastName,
        getLogin: function () {
            return generateLogin(this.firstName, this.lastName);
        }
    }
};
const testUser = createNewUser(userName, userSurname);

console.log(testUser);

console.log(testUser.getLogin());